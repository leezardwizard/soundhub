package hub.sound.com.soundhub.network.usecases;

import javax.inject.Inject;

import hub.sound.com.soundhub.model.TopArtistsResponse;
import hub.sound.com.soundhub.network.api.TopArtistApi;
import io.reactivex.Single;
import retrofit2.Retrofit;

public class GetTopArtists {

    Retrofit retrofit;

    @Inject
    public GetTopArtists(Retrofit retrofit) { this.retrofit = retrofit; }

    public Single<TopArtistsResponse> getTopArtists(String tag, String apiKey) {
        return retrofit.create(TopArtistApi.class).getTopArtists(tag, apiKey);
    }
}
