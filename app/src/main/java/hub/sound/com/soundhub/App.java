package hub.sound.com.soundhub;

import android.content.Context;
import android.support.multidex.MultiDexApplication;


import hub.sound.com.soundhub.di.AppComponent;
import hub.sound.com.soundhub.di.AppModule;
import hub.sound.com.soundhub.di.DaggerAppComponent;
import hub.sound.com.soundhub.di.NetworkModule;


public class App extends MultiDexApplication {

    private AppComponent appComponent;
    private static final String APP_LOG_TAG = "SoundHub";

    @Override
    public void onCreate() {
        super.onCreate();
        configureDependencyInjection();
    }

    private void configureDependencyInjection() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
        appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static App fromContext(Context context) {
        return (App) context.getApplicationContext();
    }
}
