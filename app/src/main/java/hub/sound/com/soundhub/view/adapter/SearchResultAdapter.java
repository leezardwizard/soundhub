package hub.sound.com.soundhub.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.ButterKnife;
import hub.sound.com.soundhub.R;
import hub.sound.com.soundhub.model.Artist;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ArtistItemHolder>{

    private List<Artist> data;
    private int lastCheckedPosition = 0;

    public SearchResultAdapter(List<Artist> data) {
        this.data = data;
    }

    @Override
    public SearchResultAdapter.ArtistItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_item, parent, false);
        return new ArtistItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchResultAdapter.ArtistItemHolder holder, int position) {
        ArtistItemHolder artistItemHolder = (ArtistItemHolder) holder;
        artistItemHolder.bind(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ArtistItemHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private ImageView image;

        ArtistItemHolder(View itemView){
            super(itemView);
            image = ButterKnife.findById(itemView, R.id.artist_image);
            name = ButterKnife.findById(itemView, R.id.artist_name);
        }

        void bind(final Artist artist, final int position){
            name.setText(artist.getName());
            Glide.with(image.getContext()).load(artist.getImageUrl()).into(image);
        }
    }

    public Artist getSelectedArtist() {
        if (lastCheckedPosition < 0 || lastCheckedPosition >= data.size()) {
            return null;
        }
        return data.get(lastCheckedPosition);
    }


}
