package hub.sound.com.soundhub.mvp;

import javax.inject.Inject;

import me.panavtec.threaddecoratedview.views.ThreadSpec;
import me.panavtec.threaddecoratedview.views.ViewInjector;

public class MvpPresenter<T extends MvpView> {

    private T view;
    private T nullObjectView;

    private ThreadSpec threadSpec;

    @Inject
    public void setThreadSpec(ThreadSpec threadSpec) {
        this.threadSpec = threadSpec;
    }

    public void attachView(T view) {
        this.view = ViewInjector.inject(view, threadSpec);
        this.nullObjectView = ViewInjector.nullObjectPatternView(view);
    }

    public void detachView() {
        view = nullObjectView;
    }

    public T getView() {
        return view;
    }

    protected void onAttach() {}

    protected void onDetach() {

    }

    protected boolean isViewAttached() {
        return view != null;
    }
}
