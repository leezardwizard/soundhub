package hub.sound.com.soundhub.network.api;


import hub.sound.com.soundhub.model.TopArtistsResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TopArtistApi {

    @GET("?method=tag.gettopartists&format=json")
    Single<TopArtistsResponse> getTopArtists(@Query("tag") String tag, @Query("api_key") String API_KEY);
}
