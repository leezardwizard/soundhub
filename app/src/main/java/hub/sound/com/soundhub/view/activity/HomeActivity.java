package hub.sound.com.soundhub.view.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import hub.sound.com.soundhub.R;
import hub.sound.com.soundhub.di.AppComponent;
import hub.sound.com.soundhub.model.Artist;
import hub.sound.com.soundhub.presenter.HomePresenter;
import hub.sound.com.soundhub.view.HomeView;
import hub.sound.com.soundhub.view.adapter.SearchResultAdapter;

public class HomeActivity extends BaseActivity implements HomeView, NavigationView.OnNavigationItemSelectedListener {

    private static final int SEARCH_BY_TAG = 1;
    private static final int SEARCH_BY_GENRE = 2;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.search_result_recyclerview)
    RecyclerView searchList;


    @Inject
    HomePresenter presenter;

    private SearchResultAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        bindPresenter(presenter,this);
        setup();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.artists_collection) {
            // Handle the camera action
        } else if (id == R.id.search_by_tag) {
            searchBy(SEARCH_BY_TAG);

        } else if (id == R.id.search_by_genre) {
           searchList.getChildCount();
            // searchBy(SEARCH_BY_GENRE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setup(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.search(query);
                searchView.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        }

    private void searchBy(int key){
        searchView.setVisibility(View.VISIBLE);
        searchView.setIconified(false);
    }

    private void updateSearchView(List<Artist> artists){
        adapter = new SearchResultAdapter(artists);
        adapter.notifyDataSetChanged();
        searchList.setLayoutManager(new GridLayoutManager(this, 2));
        searchList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        searchList.setAdapter(adapter);
    }

    @Override
    public void search(String result) {

    }

    @Override
    protected void injectDependencies(AppComponent appComponent) {
        super.injectDependencies(appComponent);
        appComponent.inject(this);
    }

    @Override
    public void topArtists(List<Artist> artists) {
        updateSearchView(artists);
    }
}
