package hub.sound.com.soundhub.di;


import javax.inject.Singleton;

import dagger.Component;
import hub.sound.com.soundhub.App;
import hub.sound.com.soundhub.view.activity.BaseActivity;
import hub.sound.com.soundhub.view.activity.HomeActivity;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(App application);

    void inject(BaseActivity activity);

    void inject(HomeActivity activity);

}
