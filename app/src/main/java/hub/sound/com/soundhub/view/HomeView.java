package hub.sound.com.soundhub.view;

import java.util.List;

import hub.sound.com.soundhub.model.Artist;
import me.panavtec.threaddecoratedview.views.qualifiers.ThreadDecoratedView;

@ThreadDecoratedView
public interface HomeView extends BaseView{

    void search(String result);

    void topArtists(List<Artist> artists);
}
