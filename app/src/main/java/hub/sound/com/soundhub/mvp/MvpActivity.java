package hub.sound.com.soundhub.mvp;

import android.support.v7.app.AppCompatActivity;

public abstract class MvpActivity extends AppCompatActivity {

    private MvpPresenter presenter;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindPresenter();
    }

    protected <T extends MvpView> void bindPresenter(MvpPresenter<T> mvpPresenter, T view) {
        this.presenter = mvpPresenter;
        mvpPresenter.attachView(view);
    }

    protected void unbindPresenter() {
        if (presenter != null) {
            presenter.detachView();
            presenter = null;
        }
    }
}
