package hub.sound.com.soundhub.view;

import hub.sound.com.soundhub.mvp.MvpView;
import me.panavtec.threaddecoratedview.views.qualifiers.ThreadDecoratedView;

@ThreadDecoratedView
public interface BaseView extends MvpView{
}
