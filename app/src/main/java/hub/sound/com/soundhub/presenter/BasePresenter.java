package hub.sound.com.soundhub.presenter;

import javax.inject.Inject;

import hub.sound.com.soundhub.mvp.MvpPresenter;
import hub.sound.com.soundhub.view.BaseView;

public abstract class BasePresenter<T extends BaseView> extends MvpPresenter<T> {
}
