package hub.sound.com.soundhub.di;

import android.content.Context;

import hub.sound.com.soundhub.App;

public class AppInjector {

    public static AppComponent from(Context context) {
        return ((App) context.getApplicationContext()).getAppComponent();
    }
}
