package hub.sound.com.soundhub.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import hub.sound.com.soundhub.di.AppComponent;
import hub.sound.com.soundhub.di.AppInjector;
import hub.sound.com.soundhub.mvp.MvpActivity;
import hub.sound.com.soundhub.presenter.AppPresenter;
import hub.sound.com.soundhub.presenter.BasePresenter;
import hub.sound.com.soundhub.view.BaseView;

public abstract class BaseActivity extends MvpActivity implements BaseView{

    @Inject
    AppPresenter presenter;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectDependencies(AppInjector.from(this));
        bindPresenter(presenter, this);

    }

    protected void onSafeCreate(@Nullable Bundle savedInstanceState) {
        //do nothing by default
    }

    protected void injectDependencies(AppComponent appComponent) {
        appComponent.inject(this);
    }
}
