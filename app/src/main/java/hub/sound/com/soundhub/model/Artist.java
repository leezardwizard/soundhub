package hub.sound.com.soundhub.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class Artist {
    @SerializedName("mbid")
    private String mbid;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private List<ImageItem> images;
    @SerializedName("streamable")
    private String streamable;
    @SerializedName("playcount")
    private String playCount;
    @SerializedName("url")
    private String url;

    public String getMbid() {
        return mbid;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ImageItem> getImages() {
        return images;
    }

    public void setImage(List<ImageItem> images) {
        this.images = images;
    }

    public String getStreamable() {
        return streamable;
    }

    public void setStreamable(String streamable) {
        this.streamable = streamable;
    }

    public String getPlaycount() {
        return playCount;
    }

    public void setPlaycount(String playcount) {
        this.playCount = playcount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        if (getImages() != null && getImages().size() > 0) {
            for (ImageItem img :
                    getImages()) {
                if (img.getSize().equalsIgnoreCase("large")) {
                    return img.getUrl();
                }
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return Objects.equals(mbid, artist.mbid) && Objects.equals(name, artist.name) && Objects.equals(images, artist.images) && Objects.equals(streamable, artist.streamable) && Objects.equals(playCount, artist.playCount) && Objects.equals(url, artist.url);
    }

    @Override
    public int hashCode() {

        return Objects.hash(mbid, name, images, streamable, playCount, url);
    }
}
