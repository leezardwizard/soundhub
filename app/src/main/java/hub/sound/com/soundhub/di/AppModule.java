package hub.sound.com.soundhub.di;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hub.sound.com.soundhub.App;
import me.panavtec.threaddecoratedview.views.ThreadSpec;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

    @Provides
    Context provideContext(App application){
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    ThreadSpec providesThreadSpec() {
        Handler handler = new Handler(Looper.getMainLooper());
        return handler::post;
    }
}
