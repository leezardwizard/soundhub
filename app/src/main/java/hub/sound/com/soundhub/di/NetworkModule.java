package hub.sound.com.soundhub.di;


import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hub.sound.com.soundhub.network.usecases.GetTopArtists;
import hub.sound.com.soundhub.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final String OK_HTTP_LOGGER = "OkHttp";

    // provides a converter factory to create the retrofit instance
    @Singleton
    @Provides
    public Converter.Factory providesConverterFactory() {
        return GsonConverterFactory.create();
    }

    // provides a call adapter factory needed to integrate rxjava with retrofit
    @Singleton
    @Provides
    public CallAdapter.Factory providesCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    // provides a retrofit instance to create the top albums interactor
    @Singleton
    @Provides
    public Retrofit providesRetrofit(Converter.Factory converter, CallAdapter.Factory adapter) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Log.d(OK_HTTP_LOGGER, message));
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(adapter)
                .addConverterFactory(converter)
                .client(client.build())
                .build();
    }

    @Singleton
    @Provides
    public GetTopArtists providesGetTopArtists(Retrofit retrofit) {
        return new GetTopArtists(retrofit);
    }


}
