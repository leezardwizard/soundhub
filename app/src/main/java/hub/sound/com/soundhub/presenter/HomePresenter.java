package hub.sound.com.soundhub.presenter;


import javax.inject.Inject;

import hub.sound.com.soundhub.model.TopArtistsResponse;
import hub.sound.com.soundhub.network.usecases.GetTopArtists;
import hub.sound.com.soundhub.view.HomeView;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static hub.sound.com.soundhub.utils.Constants.API_KEY;

public class HomePresenter extends BasePresenter<HomeView> {

    GetTopArtists getTopArtistsUseCase;

    @Inject
    HomePresenter(GetTopArtists getTopArtistsUseCase) {
        this.getTopArtistsUseCase = getTopArtistsUseCase;
    }

    public void search(String tag){
        getTopArtistsUseCase.getTopArtists(tag, API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .subscribe(new SingleObserver<TopArtistsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(TopArtistsResponse topArtistsResponse) {
                        getView().topArtists(topArtistsResponse.getTopartists().getArtists());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.getCause();
                    }
                });
    }
}
